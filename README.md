#node-red-contrib-accellion-custom-generic
### Generic Custom Accellion Nodes
Custom nodes built to work with the Accellion Content Firewall.

Currently provides the following nodes:

- *s2s*: allows to synchronize content from one Accellion platform to another
- *mail.list*: lists all secure emails for the current user account
- *mail.download*: downloads all secure email contents for the current user account
- *utils.trigger*: triggers a flow by calling a url
- *fs.file.attributes*: delivers local file attributes
- *file.sync*: unidirectional data synchronization, from Accellion to a local folder or vice-versa

For more information on the Accellion Content Firewall, please visit [Accellion](https://www.accellion.com).

### IMPORTANT NOTE
Before updating, note that the API Request node as well as all API helper nodes have been removed. The API request node is now available as part of the Accellion nodes package.
