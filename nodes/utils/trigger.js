const path = require('path');
const fs = require('fs');

module.exports = function(RED) {
    //==========================================================================
    function ACC_Utils_Trigger(config) {
        RED.nodes.createNode(this,config);
        console.log("trigger: deployed");
        var node = this;
        node.trigger={started:false,timeout:null,res:null};
        node.config=config;
        //----------------------------------------------------------------------
        node.callback=(req,res)=>{
          console.log("trigger: request");
          if (node.trigger.started) {
            console.log("trigger: already started");
            node.status({fill:"yellow",shape:"ring",text:"flow already executing"});
            return res.send({error:'Triggered while still executing flow'});
          } else {
            console.log("trigger: starting");
            node.status({fill:"green",shape:"ring",text:"flow start triggered"});
            node.trigger.started=true;
            node.trigger.res=res;
            if (node.trigger.timeout) clearTimeout(node.trigger.timeout);
            if (node.timeout>0) {
              node.trigger.timeout=setTimeout(()=>{
                console.log("trigger: timeout");
                node.status({fill:"red",shape:"dot",text:"flow timeout"});
                res.send({error:'Flow timeout'});
                node.trigger.res=null;
                node.trigger.started=false;
                node.trigger.timeout=null;
                clearTimeout(node.trigger.timeout);
              },node.config.timeout*1000);
            }
            console.log("trigger: next");
            node.send({flowStart:new Date()});
          }
        }
        //----------------------------------------------------------------------
        RED.httpNode.get("/"+node.config.key,node.callback);
        console.log("trigger: listening for "+node.config.key);
        //----------------------------------------------------------------------
        this.on("close",function() {
            var node = this;
            RED.httpNode._router.stack.forEach(function(route,i,routes) {
                if (route.route && route.route.path === "/"+node.config.key) {
                    console.log("trigger: closing "+node.config.key);
                    routes.splice(i,1);
                }
            });
        });
        //----------------------------------------------------------------------
        node.on('input', (msg)=>{
          if (node.trigger.started) {
            console.log("trigger: done");
            node.status({fill:"green",shape:"dot",text:"flow execution done"});
            clearTimeout(node.trigger.timeout);
            node.trigger.timeout=null;
            msg.flowEnd=new Date();
            node.trigger.res.send(msg);
            node.trigger.started=false;
            node.trigger.res=null;
            return;
          } else {
            console.log("trigger: flow end already triggered (timeout?)");
            node.status({fill:"red",shape:"ring",text:"flow end already triggered (timeout?)"});
          }
        });
      }
    //==========================================================================
    RED.nodes.registerType("utils.trigger",ACC_Utils_Trigger);
    //==========================================================================
}
