const path = require('path');
const fs = require('fs');
const moment = require('moment');
const Cache = require('../../lib/cache.js');
const StateManager = require('../../lib/state.js');

module.exports = function(RED) {
    //==========================================================================
    function ACC_Mail_Download(config) {
        RED.nodes.createNode(this,config);
        if (!config.token) return;
        var node = this;
        var kw = RED.nodes.getNode(config.token).kw;
        //----------------------------------------------------------------------
        node.on('input', (msg)=>{
          StateManager.waitForIdle(node.id,()=>{
            run(msg,node,config,kw);
          });
        });
        //----------------------------------------------------------------------
        function run(msg,node,config,kw) {
          StateManager.setRunning(node.id);
          //-CHECKS-------------------------------------------------------------
          msg.folder = config.folder ? config.folder : msg.folder;
          node.status({fill:"green",shape:"ring",text:"performing checks"});
          if (!kw) {
            node.status({fill:"red",shape:"dot",text:"no token"});
            StateManager.setIdle(node.id);
            return node.error("no token",msg);
          }
          if (!msg.folder) {
            node.status({fill:"red",shape:"dot",text:"destination folder not specified"});
            StateManager.setIdle(node.id);
            return node.error("destination folder not specified",msg);
          }
          if (msg.idle && !msg.id) {
            node.status({fill:"green",shape:"dot",text:"nothing to do"});
            StateManager.setIdle(node.id);
            return node.send(msg);
          }

          //-ACTION-------------------------------------------------------------
          try {
            node.status({fill:"green",shape:"ring",text:"processing email "+msg.id});

            downloadEmail().then(()=>{
              downloadFiles().then(()=>{
                markAsRead().then(()=>{
                  StateManager.setIdle(node.id);
                  node.status({fill:"green",shape:"dot",text:"email downloaded"});
                  return node.send(msg);
                },(err)=>{
                  StateManager.setIdle(node.id);
                  node.status({fill:"yellow",shape:"dot",text:"warning: could not mark email as read: "+err});
                  return node.send(msg);
                });
              },(err)=>{
                StateManager.setIdle(node.id);
                node.status({fill:"red",shape:"dot",text:"warning: could not download email files: "+err});
                return node.error("could not download email files for "+msg.id+": "+err,msg);
              });
            },(err)=>{
              StateManager.setIdle(node.id);
              node.status({fill:"red",shape:"dot",text:"warning: could not download email: "+err});
              return node.error("could not download email "+msg.id+": "+err,msg);
            });
            //------------------------------------------------------------------
            function markAsRead(){
              return new Promise((resolve, reject)=>{
                if (!config.markAsRead) return resolve();
                kw.mail.markAsRead(msg.id).then((res)=>{
                  return resolve(res);
                },(err)=>{
                  return reject(err);
                });
              });
            }
            //------------------------------------------------------------------
            function downloadEmail(){
              return new Promise((resolve, reject)=>{
                if (!config.email) return resolve();
                if (!fs.existsSync(config.folder)) fs.mkdirSync(config.folder, { recursive: true });
                try {
                  fs.writeFileSync(path.join(config.folder,msg.id+"-email.json"), JSON.stringify(msg));
                  return resolve();
                } catch (err) {
                  return reject(err);
                }
              });
            }
            //------------------------------------------------------------------
            function downloadFiles(){
              return new Promise((resolve, reject)=>{
                if (!config.files) return resolve();

                //check file count
                var fileCount=msg.variables.find((el)=>{return el.variable=="FILE_COUNT"});
                if (!fileCount || !fileCount.value || parseInt(fileCount.value)<=0) {
                  node.status({fill:"green",shape:"ring",text:"email "+msg.id+" has no attachments"});
                  return resolve();
                }
                fileCount.value=parseInt(fileCount.value);

                //check expiration date
                var expiration=msg.variables.find((el)=>{return el.variable=="EXPIRATION_DATE"});
                if (expiration && expiration.value && moment(expiration.value,"MMM D, YYYY").isBefore(moment())) {
                  node.status({fill:"green",shape:"ring",text:"email "+msg.id}+" attachments have expired");
                  return resolve();
                }
                //retrieve email
                node.status({fill:"green",shape:"ring",text:"retrieving email attachments for "+msg.id});
                kw.mail.getAttachments(msg.id).then((attachments)=>{
                  if (!attachments || !attachments.data) {
                    node.status({fill:"green",shape:"ring",text:"email "+msg.id}+" has no attachment list");
                    return resolve();
                  }
                  attachments=attachments.data;

                  (function dl(config,msg,list,idx,done){
                    if (idx>=list.length) return done();
                    node.status({fill:"green",shape:"ring",text:"downloading file "+(idx+1)+" of "+list.length+" for email "+msg.id});

                    kw.files.find.byId(list[idx].originalFileId).then((file)=>{
                      kw.files.download.byId(list[idx].originalFileId,config.folder, msg.id+"-"+file.name).then((res)=>{
                        node.status({fill:"green",shape:"ring",text:"downloaded file "+file.name});
                        idx++;
                        dl(config,msg,list,idx,done);
                      },(err)=>{
                        node.status({fill:"yellow",shape:"ring",text:"file download error: "+file.name});
                        idx++;
                        dl(config,msg,list,idx,done);
                      });
                    },(err)=>{
                      node.status({fill:"yellow",shape:"ring",text:"file not found: "+list[idx].originalFileId});
                      idx++;
                      dl(config,msg,list,idx,done);
                    });
                  })(config,msg,attachments,0,resolve);
                },(err)=>{
                  return reject(err);
                });
              });
            }
            //------------------------------------------------------------------
          } catch (err) {
            StateManager.setIdle(node.id);
            node.status({fill:"red",shape:"dot",text:"error: "+err});
            return node.error("error: "+err,msg);
          }
          //-DONE---------------------------------------------------------------
        };
        //----------------------------------------------------------------------
      };
    //==========================================================================
    RED.nodes.registerType("mail.download",ACC_Mail_Download);
    //==========================================================================
}
