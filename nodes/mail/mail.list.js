const path = require('path');
const fs = require('fs');
const Cache = require('../../lib/cache.js');
const StateManager = require('../../lib/state.js');

module.exports = function(RED) {
    //==========================================================================
    function ACC_Mail_List(config) {
        RED.nodes.createNode(this,config);
        if (!config.token) return;
        var node = this;
        var kw = RED.nodes.getNode(config.token).kw;
        //----------------------------------------------------------------------
        node.on('input', (msg)=>{

          node.status({fill:"green",shape:"ring",text:"performing checks"});
          if (!kw) {
            node.status({fill:"red",shape:"dot",text:"no token"});
            StateManager.setIdle(node.id);
            return node.error("no token",msg);
          }

          node.status({fill:"green",shape:"ring",text:"listing emails"});
          var obj={read:config.read, deleted:config.deleted,isPreview:config.isPreview, bucket: config.bucket}
          if (config.mailStatus && config.mailStatus!="") obj.status=config.mailStatus;
          kw.mail.list(obj).then((r)=>{
            if (!r || !r.data) {
              node.status({fill:"red",shape:"dot",text:"error: no email list found"});
              return node.error("error: no email list found",msg);
            }
            node.status({fill:"green",shape:"dot",text:"email list retrieved"});

            if (r.data.length==0) {
              return node.send([[{isLast:true,idle:true}]]);
            }
            r.data[r.data.length-1].isLast=true;
            return node.send([r.data,{count:r.data.length}]);
          },(err)=>{
            node.status({fill:"red",shape:"dot",text:"error: "+err});
            return node.error("error: "+err,msg);
          });
        });
      };
    //==========================================================================
    RED.nodes.registerType("mail.list",ACC_Mail_List);
    //==========================================================================
}
