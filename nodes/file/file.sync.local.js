const path = require('path');
const fs = require('fs');
const md5File = require("md5-file");
const moment = require("moment");
const Threads = require("../../lib/threads.js");
//==========================================================================
function createMap (localFolder,accFolder, doneFn, errFn, fileMap, folderMap) {
    fileMap = fileMap || [];
    folderMap = folderMap || [];
    try {
        var objects = fs.readdirSync(localFolder);
        objects=objects.filter((el)=>{return !el.startsWith(".")});
        objects.forEach(function(object) {
            var attributes = fs.statSync(path.join(localFolder,object));
            if (attributes.isFile()) {
                fileMap.push({
                    localFolder: localFolder, 
                    accFolder: accFolder,
                    name: object, 
                    localModified: moment(attributes.mtime), // mtime or ctime?
                    localMd5: md5File.sync(path.join(localFolder,object)),
                    state: "LOCAL-ONLY"
                });
            } else if (attributes.isDirectory()) {
                folderMap.push({localFolder: path.join(localFolder,object), accFolder: `${accFolder}/${object}`,state:"QUEUED"});
            }           
        });            
        var nextFolder = folderMap.find((el)=>{return el.state=="QUEUED"}); 
        if (!nextFolder) return doneFn(fileMap,folderMap);
        nextFolder.state="PROCESSED-LOCAL";
        createMap(nextFolder.localFolder, nextFolder.accFolder, doneFn, errFn, fileMap, folderMap);
    } catch (err) {
        return errFn(err);
    }
}
//==========================================================================
function createStructure(folderMap,doneFn,errFn) {
    try {
        folderMap.forEach((folder)=>{
            if (!fs.existsSync(folder.localFolder)) fs.mkdirSync(folder.localFolder,{recursive:true});
        });
        return doneFn();
    } catch (err) {
        return errFn(err);
    }
}
//==========================================================================
function syncFiles (node,kw,maxThreads,fileMap,doneFn) {
    var fileQueue = fileMap.filter((el)=>{
        if (el.state=="REMOTE-ONLY") return true; 
        else if (el.state=="NEW-HASH" && moment(el.accModified).isAfter(moment(el.localModified))) return true;
        return false;
    });
    node.status({fill:"green",shape:"ring",text: `downloading ${fileQueue.length} files`});
    var pool = Threads.createPool(`file.sync.pool.down.${node.id}`,maxThreads);
    fileQueue.forEach((file)=>{pool.add(__downloadFile,{node:node,file:file,kw:kw});});
    pool.run();
    function __checkPool(){
        if (pool.done()) return doneFn(fileMap);
        else setTimeout(__checkPool,500);
    }
    __checkPool();
}
//==========================================================================
function downloadFiles (node,kw,maxThreads,fileMap,doneFn) {
    var fileQueue = fileMap.filter((el)=>{return el.state=="REMOTE-ONLY" || el.state=="NEW-HASH"});
    node.status({fill:"green",shape:"ring",text: `downloading ${fileQueue.length} files`});
    var pool = Threads.createPool(`file.sync.pool.down.${node.id}`,maxThreads);
    fileQueue.forEach((file)=>{pool.add(__downloadFile,{node:node,file:file,kw:kw});});
    pool.run();
    function __checkPool(){
        if (pool.done()) return doneFn(fileMap);
        else setTimeout(__checkPool,500);
    }
    __checkPool();
}
//==========================================================================
function __downloadFile(thread,context,done) {
    context.kw.files.download.byId(context.file.accId,context.file.localFolder,context.file.name).then((res)=>{
        context.file.state="SYNCED";
        context.file.localMd5=md5File.sync(path.join(context.file.localFolder,context.file.name));
        context.node.send([null,{file:context.file},null]);
        return done(thread);
    },(err)=>{
        context.file.state="ERROR";
        context.node.send([null,null,{file:context.file,error:err}]);
        return done(thread);
    });
}
//==========================================================================
function deleteFiles(fileMap,done) {
    var toDelete=fileMap.filter((el)=>{return el.state=="LOCAL-ONLY"});
    toDelete.forEach((file)=>{
        try {
            fs.unlinkSync(path.join(file.localFolder,file.name));
            file.state="DELETED";
        } catch(err) {
            node.send([null,null,{error:err,file:file}]);
        }
    });
    return done(fileMap);
}
//==========================================================================
module.exports={
    createMap: createMap,
    createStructure: createStructure,
    downloadFiles: downloadFiles,
    deleteFiles: deleteFiles,
    syncFiles: syncFiles
};