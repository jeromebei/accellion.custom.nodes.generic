const path = require('path');
const fs = require('fs');

module.exports = function(RED) {
    //==========================================================================
    function ACC_Fs_File_Attributes(config) {
        RED.nodes.createNode(this,config);
        var node = this;
        //----------------------------------------------------------------------
        node.on('input', (msg)=>{
          //-CHECKS-------------------------------------------------------------
          node.status({fill:"green",shape:"ring",text:"performing checks"});
          if (!msg.filePath) {
            node.status({fill:"red",shape:"dot",text:"no file in msg"});
            StateManager.setIdle(node.id);
            return node.error("no file in msg",msg);
          }
          if (!fs.existsSync(msg.filePath)) {
            node.status({fill:"red",shape:"dot",text:"file does not exist: "+msg.filePath});
            StateManager.setIdle(node.id);
            return node.error("file does not exist: "+msg.filePath,msg);
          }
          //-ACTION-------------------------------------------------------------
          try {
            node.status({fill:"green",shape:"ring",text:"checking attributes"});
            const file_fd = fs.openSync(msg.filePath, 'r');
            msg.fileAttributes = fs.fstatSync(file_fd);
            delete msg.fileAttributes.atimeMs;
            delete msg.fileAttributes.mtimeMs;
            delete msg.fileAttributes.ctimeMs;
            delete msg.fileAttributes.birthtimeMs;
            msg.fileAttributes.accessTime = msg.fileAttributes.atime;
            delete msg.fileAttributes.atime;

            msg.fileAttributes.accessedTime = msg.fileAttributes.atime;
            delete msg.fileAttributes.atime;

            msg.fileAttributes.modifiedTime = msg.fileAttributes.mtime;
            delete msg.fileAttributes.mtime;

            msg.fileAttributes.changedTime = msg.fileAttributes.ctime;
            delete msg.fileAttributes.ctime;

            msg.fileAttributes.createdTime = msg.fileAttributes.birthtime;
            delete msg.fileAttributes.birthtime;

            fs.closeSync(file_fd);
            node.status({fill:"green",shape:"dot",text:"done"});
            return node.send(msg);
          } catch (err) {
            node.status({fill:"red",shape:"dot",text:"error: "+err});
            return node.error("error: "+err,msg);
          }
        });
      }
    //==========================================================================
    RED.nodes.registerType("fs.file.attributes",ACC_Fs_File_Attributes);
    //==========================================================================
}
