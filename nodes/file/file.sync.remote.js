const path = require('path');
const fs = require('fs');
const md5File = require("md5-file");
const moment = require("moment");
const Trail = require("../../lib/trail.js");
const Threads = require("../../lib/threads.js");

//==========================================================================
function createMap (localFolder, accFolder, kw, fileMap, folderMap, doneFn, errFn) {
    kw.folders.find.byName(accFolder).then((accRootFolder)=>{
        var folder = folderMap.find((el)=>{return el.accFolder==accFolder});
        if (folder) {
            folder.accId=accRootFolder.id;
            folder.state="PROCESSED-REMOTE"            
        } else {
            folderMap.push({
                accId: accRootFolder.id,
                accFolder: accFolder,
                localFolder: localFolder,
                state: "PROCESSED-REMOTE"
            });
        }
        __createMap (localFolder,accFolder, accRootFolder.id, kw, doneFn, errFn, fileMap, folderMap);
    },(err)=>{
        return errFn(err);
    });
}
//==========================================================================
function __createMap (localFolder, accFolder, accFolderId, kw, doneFn, errFn, fileMap, folderMap) {
    fileMap = fileMap || [];
    var folderMap = folderMap || [];
    kw.folders.children.byId(accFolderId).then((list)=>{
        var __objects= list.data.filter((el)=>{return !el.deleted && !el.permDeleted});
        __objects.forEach((__object)=>{
            if (__object.type=="f") {
                var __file = fileMap.find((f)=>{return f.name==__object.name && f.accFolder==accFolder && f.localMd5==__object.fingerprint});  
                if (__file) {
                    __file.accFolderId=accFolderId;
                    __file.accId=__object.id;
                    __file.accMd5=__object.fingerprint;
                    __file.accClientModified=moment(__object.clientModified);
                    __file.accModified=moment(__object.modified);
                    __file.state="SYNCED";
                } else {
                    __file = fileMap.find((f)=>{return f.name==__object.name && f.accFolder==accFolder});  
                    if (__file) {
                        __file.accFolderId=accFolderId;
                        __file.accId=__object.id;
                        __file.accMd5=__object.fingerprint;
                        __file.accClientModified=moment(__object.clientModified);
                        __file.accModified=moment(__object.modified);
                        __file.state="NEW-HASH";
                    } else {
                        __file = {
                            name: __object.name,
                            accId: __object.id,
                            accFolder: accFolder,
                            accFolderId: accFolderId,
                            accMd5: __object.fingerprint,
                            localFolder: localFolder,
                            accClientModified: moment(__object.clientModified),
                            accModified: moment(__object.modified),
                            state: "REMOTE-ONLY"
                        }
                        fileMap.push(__file);
                    }
                }
            } else if (__object.type=="d") {
                var folder = folderMap.find((el)=>{return el.accFolder==`${accFolder}/${__object.name}`});
                if (folder) {
                    folder.accId=__object.id;
                    folder.state="QUEUED"
                } else {
                    folderMap.push({
                        accId: __object.id,
                        accFolder: `${accFolder}/${__object.name}`,
                        localFolder: path.join(localFolder,__object.name),
                        state: "QUEUED"
                    });
                }
            }
        });

        var nextFolder = folderMap.find((el)=>{return el.state=="QUEUED"}); 
        
        if (!nextFolder) {
            fileMap=fileMap.map((f)=>{
                if (!f.accFolderId) {
                    var remoteFolder = folderMap.find((el)=>{return el.accFolder==f.accFolder});
                    if (remoteFolder) f.accFolderId=remoteFolder.accId;
                }
                return f;
            });
            return doneFn(fileMap,folderMap);
        };
        nextFolder.state="PROCESSED-REMOTE";
        __createMap(nextFolder.localFolder, nextFolder.accFolder, nextFolder.accId, kw, doneFn, errFn, fileMap, folderMap);

    },(err)=>{
        return errFn(err);
    });
}
//==========================================================================
function createStructure(accRootFolder,localRootFolder,fileMap,folderMap,kw,doneFn,errFn) {
    for (var item of folderMap) {
        if (item.accFolder==accRootFolder) continue;
        var tokens = item.accFolder.split("/");
        while (typeof(tokens.pop())!="undefined") {
            if (tokens.length>0) {
                var str = tokens.join("/");
                if (!folderMap.find((el)=>{return el.accFolder==str})){
                    folderMap.push({
                        accFolder:str,
                        localFolder: path.join(localRootFolder,[...tokens]),
                        accId:null,
                        state:"PROCESSED-LOCAL"
                    });
                }
                if (str==accRootFolder) break;
            }
        }
    }
    folderMap.sort((a,b)=>{return a.accFolder>b.accFolder ? 1 : -1});
    //console.log(folderMap)
    __createFolders(kw,fileMap,folderMap,(fileMap,folderMap)=>{
        return doneFn(fileMap,folderMap);
    },(err)=>{
        return errFn(err);
    });
}
//==========================================================================
function __createFolders(kw,fileMap,folderMap,doneFn,errFn,idx) {
    if (!idx) idx=0;
    if (idx>=folderMap.length) return doneFn(fileMap,folderMap);
    var folder = folderMap[idx];
    if (folder.accId!=null) {idx++; return __createFolders(kw,fileMap,folderMap,doneFn,errFn,idx);}
    var parentName = folder.accFolder.split("/");
    var folderName=parentName.pop();
    parentName=parentName.join("/");
    var parentFolder=folderMap.find((f)=>{return f.accFolder==parentName;});
    if (!parentFolder) return errFn(`parent not found for ${folder.accFolder}`);
    kw.folders.add.byId({parentId:parentFolder.accId,name:folderName}).then((res)=>{
        folderId=res.id;
        folderMap=folderMap.map((f)=>{if (f.accFolder==folder.accFolder) f.accId=res.id;f.state="PROCESSED-REMOTE"; return f;});
        fileMap=fileMap.map((f)=>{if (f.accFolder==folder.accFolder) f.accFolderId=res.id;return f;});
        idx++;
        return __createFolders(kw,fileMap,folderMap,doneFn,errFn,idx);
    },(err)=>{
        return errFn(`cannot create parent: ${folder.accFolder}, error: ${err}`);   
    });
}
//==========================================================================
function syncFiles (node,kw,maxThreads,fileMap,doneFn) {
    var fileQueue = fileMap.filter((el)=>{
        if (el.state=="LOCAL-ONLY") return true; 
        else if (el.state=="NEW-HASH" && moment(el.localModified).isAfter(moment(el.accModified))) return true;
        return false;
    });    
    node.status({fill:"green",shape:"ring",text: `uploading ${fileQueue.length} files`});
    var pool = Threads.createPool(`file.sync.pool.up.${node.id}`,maxThreads);
    fileQueue.forEach((file)=>{pool.add(__uploadFile,{node:node,file:file,kw:kw});});
    pool.run();
    function __checkPool(){
        if (pool.done()) return doneFn(fileMap);
        else setTimeout(__checkPool,500);
    }
    __checkPool();
}  
//==========================================================================
function uploadFiles (node,kw,maxThreads,fileMap,doneFn) {
    var fileQueue = fileMap.filter((el)=>{return el.state=="LOCAL-ONLY" || el.state=="NEW-HASH"});
    node.status({fill:"green",shape:"ring",text: `uploading ${fileQueue.length} files`});
    var pool = Threads.createPool(`file.sync.pool.up.${node.id}`,maxThreads);
    fileQueue.forEach((file)=>{pool.add(__uploadFile,{node:node,file:file,kw:kw});});
    pool.run();
    function __checkPool(){
        if (pool.done()) return doneFn(fileMap);
        else setTimeout(__checkPool,500);
    }
    __checkPool();
}   
//==========================================================================
function __uploadFile(thread,context,done) {
    var fakenode = {status:(opts)=>{return;}}; //we don't want the upload function to write status messages
    context.kw.files.upload.byFolderId(context.file.accFolderId, path.join(context.file.localFolder,context.file.name), fakenode, Trail).then((res)=>{
        context.file.state="SYNCED";
        context.file.accMd5=res.fingerprint;
        context.file.accId=res.id
        context.node.send([null,{file:context.file},null]);
        return done(thread);
    },(err)=>{
        context.file.state="ERROR";
        context.node.send([null,null,{file:context.file,error:err}]);
        return done(thread);
    });
}         
//==========================================================================
function deleteFiles(node,kw,maxThreads,fileMap,doneFn) {
    var fileQueue=fileMap.filter((el)=>{return el.state=="REMOTE-ONLY"});
    var pool = Threads.createPool(`file.sync.pool.del.${node.id}`,maxThreads);
    fileQueue.forEach((file)=>{pool.add(__deleteFile,{node:node,file:file,kw:kw});});
    pool.run();
    function __checkPool(){
        if (pool.done()) return doneFn(fileMap);
        else setTimeout(__checkPool,500);
    }
    __checkPool();    
}
//==========================================================================
function __deleteFile(thread,context,doneFn) {
    context.kw.files.delete.byId(context.file.accId).then((res)=>{
        context.file.state="DELETED";
        return doneFn(thread);
    },(err)=>{
        context.file.state="ERROR";
        context.node.send([null,null,{file:context.file,error:err}]);
        return doneFn(thread);
    });
}         
//==========================================================================
module.exports={
    createMap: createMap,
    createStructure: createStructure,
    uploadFiles: uploadFiles,
    deleteFiles: deleteFiles,
    syncFiles: syncFiles
};