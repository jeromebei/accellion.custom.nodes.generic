const path = require('path');
const fs = require('fs');
const md5File = require("md5-file");
const moment = require("moment");
const Threads = require("../../lib/threads.js");
const syncLocal = require("./file.sync.local.js");
const syncRemote = require("./file.sync.remote.js");

module.exports = function(RED) {
    //==========================================================================
    function ACC_File_Sync(config) {
        RED.nodes.createNode(this,config);
        var node = this;
        var running=false;
        var startMoment=null;
        var endMoment=null;
        //----------------------------------------------------------------------
        node.on('input', (msg)=>{
            if (running) return node.send([null,null,{error:"still running"}]);
            running=true;
            startMoment=moment();
            msg.accRootFolder = msg.accRootFolder || config.accRootFolder;
            msg.fsRootFolder  = msg.fsRootFolder  || config.fsRootFolder;
            msg.threads = parseInt(config.threads) < 1 ? 1 : parseInt(config.threads);
            msg.keepExisting = config.keepExisting;
            msg.direction = config.direction || "DOWN";
            if (!msg.accRootFolder) return handleError(node,"accellion root folder not specified",msg);
            if (!msg.fsRootFolder) return handleError(node,"local root folder not specified",msg);
            if (!config.token) return handleError(node,"token not specified",msg);
            try {
                if (!fs.existsSync(msg.fsRootFolder)) fs.mkdirSync(msg.fsRootFolder,{recursive:true});
            } catch (e) {
                return handleError(node,"Could not create local root folder",msg);
            }
            node.status({fill:"green",shape:"ring",text:"creating local map..."});
            //LOCAL MAP - (error ends the flow)
            node.status({fill:"green",shape:"ring",text: `creating local map`});
            syncLocal.createMap(msg.fsRootFolder,msg.accRootFolder, (fileMap,folderMap)=>{
                var kw = RED.nodes.getNode(config.token).kw;
                //REMOTE MAP - (error ends the flow)
                node.status({fill:"green",shape:"ring",text: `creating remote map`});
                syncRemote.createMap(msg.fsRootFolder,msg.accRootFolder, kw, fileMap, folderMap, (fileMap,folderMap)=>{
                    msg.files=fileMap;
                    msg.folders=folderMap;
                    delete msg.duration;
                    node.send([null,msg,null]);
                    // --------- DOWN ------------------------------------------------
                    if (msg.direction=="DOWN") {
                        //LOCAL STRUCTURE - (error ends the flow)
                        node.status({fill:"green",shape:"ring",text: `creating local structure`});
                        syncLocal.createStructure(folderMap,()=>{
                            //DOWNLOAD - (error continues with next element)
                            syncLocal.downloadFiles(node,kw,config.threads,fileMap,(fileMap)=>{
                                if (!msg.keepExisting) {
                                    node.status({fill:"green",shape:"ring",text: `deleting files`});
                                    syncLocal.deleteFiles(fileMap,(fileMap)=>{
                                        node.status({fill:"green",shape:"dot",text: `all done`});
                                        msg.files=fileMap;
                                        msg.folders=folderMap;
                                        running=false;
                                        endMoment=moment();
                                        msg.duration=endMoment.diff(startMoment,'seconds');
                                        return node.send([msg,null,null]);
                                    });
                                } else {
                                    node.status({fill:"green",shape:"dot",text: `all done`});
                                    msg.files=fileMap;
                                    msg.folders=folderMap;
                                    running=false;
                                    endMoment=moment();
                                    msg.duration=endMoment.diff(startMoment,'seconds');
                                    return node.send([msg,null,null]);
                                }
                            });
                        },(err)=>{
                            return handleError(node,`could not create local structure: ${err}`,msg);
                        });
                    // --------- UP --------------------------------------------------
                    } else if (msg.direction=="UP") {
                        //REMOTE STRUCTURE - (error ends the flow)
                        node.status({fill:"green",shape:"ring",text: `creating remote structure`});
                        syncRemote.createStructure(msg.accRootFolder,msg.localFolder, fileMap,folderMap,kw,(fileMap,folderMap)=>{
                            syncRemote.uploadFiles(node,kw,config.threads,fileMap,(fileMap)=>{
                                if (!msg.keepExisting) {
                                    node.status({fill:"green",shape:"ring",text: `deleting files`});
                                    syncRemote.deleteFiles(node,kw,config.threads,fileMap,(fileMap)=>{
                                        node.status({fill:"green",shape:"dot",text: `all done`});
                                        msg.files=fileMap;
                                        msg.folders=folderMap;
                                        running=false;
                                        endMoment=moment();
                                        msg.duration=endMoment.diff(startMoment,'seconds');
                                        return node.send([msg,null,null]);
                                    });
                                } else {
                                    node.status({fill:"green",shape:"dot",text: `all done`});
                                    msg.files=fileMap;
                                    msg.folders=folderMap;
                                    running=false;
                                    endMoment=moment();
                                    msg.duration=endMoment.diff(startMoment,'seconds');
                                    return node.send([msg,null,null]);
                                }
                            });
                        },(err)=>{
                            return handleError(node,`could not create remote structure: ${err}`,msg);
                        });
                    // --------- TWO-WAY-----------------------------------------------
                    } else if (msg.direction=="TWO-WAY") {
                        //LOCAL STRUCTURE - (error ends the flow)
                        node.status({fill:"green",shape:"ring",text: `creating local structure`});
                        syncLocal.createStructure(folderMap,()=>{
                            //REMOTE STRUCTURE - (error ends the flow)
                            node.status({fill:"green",shape:"ring",text: `creating remote structure`});
                            syncRemote.createStructure(msg.accRootFolder,msg.localFolder, fileMap,folderMap,kw,(fileMap,folderMap)=>{
                                syncLocal.syncFiles(node,kw,config.threads,fileMap,(fileMap)=>{
                                    syncRemote.syncFiles(node,kw,config.threads,fileMap,(fileMap)=>{
                                        node.status({fill:"green",shape:"dot",text: `all done`});
                                        msg.files=fileMap;
                                        msg.folders=folderMap;
                                        running=false;
                                        endMoment=moment();
                                        msg.duration=endMoment.diff(startMoment,'seconds');
                                        return node.send([msg,null,null]);
                                    });
                                });
                            },(err)=>{
                                return handleError(node,`could not create remote structure: ${err}`,msg);
                            });                        
                        },(err)=>{
                            return handleError(node,`could not create local structure: ${err}`,msg);
                        });                        
                // --------- UNKNOWN ---------------------------------------------
                    } else {
                        return handleError(node,`unknown sync direction: ${msg.direction}`,msg);
                    }
                },(err)=>{
                    return handleError(node,`could not create remote map: ${err}`,msg);
                });
            },(err)=>{
                return handleError(node,`could not create local map: ${err}`,msg);
            });

        });
        //----------------------------------------------------------------------
        function handleError(node,err,msg) {
            node.status({fill:"red",shape:"dot",text:err});
            node.error(err,msg);
            if (!msg) msg={};
            msg.error=err;
            return node.send([null,null,msg]);
        }
        //----------------------------------------------------------------------
    }
    //==========================================================================
    RED.nodes.registerType("file.sync",ACC_File_Sync);
    //==========================================================================
}
