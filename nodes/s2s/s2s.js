const path = require('path');
const fs = require('fs');
const request = require('request');
const rimraf = require('rimraf');
//==============================================================================
//==============================================================================
const Threads=require("../../lib/threads.js");
const Sanitizer=require("./sanitizer.js");
//==============================================================================
module.exports = function(RED) {
  //============================================================================
  SCAN_STATE={"UNKNOWN":0, "INQUEUE":1,"INSCAN":2,"FINISHED":3,"RECEIVED":4,"PRESCAN":5};
  SCAN_RESULT={"OK":0, "DROP":1,"RECONSTRUCTED":2,"ERROR":3};
  //============================================================================
  function Acc_Generic_S2S(config) {
    RED.nodes.createNode(this,config);
    var node=this;
    var sanitizer = new Sanitizer(config,node,RED);
    //--------------------------------------------------------------------------
    // INPUT
    //--------------------------------------------------------------------------
    node.on('input', function(msg) {
      sanitizer.run(msg,(opts,cb)=>{
        var files = sanitizer.filterFiles(Sanitizer.FILE_STATE.DOWNLOADED);
        for (var i = 0; i < files.length; i++) {
          file = files[i].value;
          file.state=Sanitizer.FILE_STATE.SANITIZED;
          file.sanitizedName=file.name;
          if (!fs.existsSync(file.folder.tempSanitizeFolder)) fs.mkdirSync(file.folder.tempSanitizeFolder,{recursive:true});
          fs.copyFileSync(path.join(file.folder.tempDownloadFolder,file.name),path.join(file.folder.tempSanitizeFolder,file.name));
        }
        return cb();
      });
      //------------------------------------------------------------------------
    });
    //--------------------------------------------------------------------------
  }
  //----------------------------------------------------------------------------
  //============================================================================
  RED.nodes.registerType("server2server",Acc_Generic_S2S);
  //============================================================================
}
//==============================================================================
