const querystring = require('querystring');
const https = require('https');
const fs = require('fs');
const path = require('path');
const { MultipartSync: Multipart } = require('multi-part');
//------------------------------------------------------------------------------
function ACCELLION_CUSTOM(kw) {
  //----------------------------------------------------------------------------
  var ac=this;
  //----------------------------------------------------------------------------
  ac.kw=kw;
  ac.__accessToken=null;
  //----------------------------------------------------------------------------
  ac.getToken=()=>{
    return new Promise ((resolve,reject)=>{
      if (ac.__accessToken!=null) return resolve(ac.__accessToken);
      (async () => {
        ac.__accessToken = await ac.kw.accessToken(ac.kw.token.serverUrl, ac.kw.token.emailId);
        return resolve(ac.__accessToken);
      })();
    },(err)=>{
      return reject(err);
    });
  };
  //----------------------------------------------------------------------------
  ac.options=(endpoint,method,form)=>{
    var formData = form ? querystring.stringify(form) : null;
    var options = {
      hostname: ac.kw.token.serverUrl.split("//")[1],
      port: 443,
      path: endpoint,
      method: method,
      headers: {
        'X-Accellion-Version': 16,
        "Accept": "application/json",
        "Authorization": "Bearer "+ac.__accessToken
      }
    }
    if (method=="POST") options.headers["Content-Type"]="application/x-www-form-urlencoded";
    if (formData) options.headers["Content-Length"]=formData.length;
    return options;
  };
  //----------------------------------------------------------------------------
  ac.api={
    //--------------------------------------------------------------------------
    get:(endpoint)=>{
      return new Promise ((resolve,reject)=>{
        try {
          var data = "";
          const req = https.request(ac.options(endpoint,'GET'), res => {
            res.on('data', d => {data += d.toString('utf8') });
            res.on('end',()=>{
              try {
                return resolve(JSON.parse(data));
              } catch (e) {
                return resolve(data);
              }
            });
          });
          req.end();
        } catch (e) {
          return reject(e);
        }
      });
    },
    //--------------------------------------------------------------------------
    post:(endpoint, form)=>{
      return new Promise ((resolve,reject)=>{
        try {
          var data = "";
          const req = https.request(ac.options(endpoint,'POST',form), res => {
            res.on('data', d => { data += d.toString('utf8') });
            res.on('end', d => {
              try {
                return resolve(JSON.parse(data));
              } catch (e) {
                return resolve(data);
              }
            });
          });
          req.write(querystring.stringify(form));
          req.end();
        } catch (e) {
          return reject(e);
        }
      });
    },
    //--------------------------------------------------------------------------
    chunk:(endpoint, form)=>{
      return new Promise ((resolve,reject)=>{
        try {
          var data = "";
          const multipartForm = new Multipart();
          for (var i = 0; i < Object.keys(form).length; i++) {multipartForm.append(Object.keys(form)[i],form[Object.keys(form)[i]]);}
          var options = {
            hostname: ac.kw.token.serverUrl.split("//")[1],
            port: 443,
            path: endpoint,
            method: "POST",
            headers: {
              'X-Accellion-Version': 16,
              "Accept": "application/json",
              "Accept-Encoding": "gzip, deflate, br",
              "Content-Length": multipartForm.getLength(),
              "Authorization": "Bearer "+ac.__accessToken,
              "Connection": "keep-alive",
              "Transfer-Encoding": "chunked",
              "Content-Type": multipartForm.getHeaders()["content-type"]
            }
          }

          const req = https.request(options, res => {
            res.on('data', d => { data += d.toString('utf8') });
            res.on('end', d => {
              try {
                return resolve(JSON.parse(data));
              } catch (e) {
                return resolve(data);
              }
            });
          });
          (async () => {
            const body = await multipartForm.buffer();
            req.write(body);
            req.end();
          })();
        } catch (e) {
          return reject(e);
        }
      });
    },
    //--------------------------------------------------------------------------
    query:(options)=>{
      var __query = "";
      if (options) for (var key in options) { if (__query != "") __query += "&"; __query += key + "=" + encodeURIComponent(options[key]); }
      if (__query!="") __query = "?"+ __query;
      return __query;
    },
    //--------------------------------------------------------------------------
  };
  //----------------------------------------------------------------------------
  ac.folders={
    //--------------------------------------------------------------------------
    list:(id,options)=>{
      var endpoint = "/rest/folders/top";
      if (id) endpoint="/rest/folders/"+id+"/children";
      var query = ac.api.query(options);
      return ac.api.get(endpoint + query);
    },
    //--------------------------------------------------------------------------
  };
  //----------------------------------------------------------------------------
  ac.favorites={
    //--------------------------------------------------------------------------
    list:(options)=>{
      var endpoint = "/rest/favorites";
      var query = ac.api.query(options);
      return ac.api.get(endpoint + query);
    },
    //--------------------------------------------------------------------------
  };
  //----------------------------------------------------------------------------
  ac.files={
    //--------------------------------------------------------------------------
    cancelledUploads:[],
    //--------------------------------------------------------------------------
    checkCancelled:(file)=>{
      if (ac.files.cancelledUploads.indexOf(file)>=0) {
        ac.files.cancelledUploads=ac.files.cancelledUploads.filter((f)=>{return f!=file});
        return true;
      } else {
        return false;
      }
    },
    //--------------------------------------------------------------------------
    upload:(folderId,file, chunkSize, progress)=>{
      return new Promise ((resolve,reject)=>{
        if (!fs.existsSync(file)) return reject("file does not exist: "+file);
        var filename    = path.basename(file);
        var stats       = fs.statSync(file);
        var totalSize   = stats["size"];
        var totalChunks = Math.ceil(totalSize / (chunkSize*1024));
        var form        = {filename:filename, totalChunks:totalChunks, totalSize:totalSize};
        console.log("upload for "+file+" will have "+totalChunks+" chunks");
        if (ac.files.checkCancelled(file)) return resolve("cancelled by user");
        ac.api.post("/rest/folders/"+folderId+"/actions/initiateUpload?returnEntity=true",form).then((response)=>{
          if (response.errors) return reject(response.errors);
          console.log("initiated upload for "+file);
          if (ac.files.checkCancelled(file)) return resolve("cancelled by user");
          ac.api.get("/rest/uploads/"+response.id+"?returnEntity=true").then((response)=>{
            if (response.errors) return reject(response.errors);
            var uri = response.uri;
            console.log("got upload uri for "+file+": "+uri);
            if (ac.files.checkCancelled(file)) return resolve("cancelled by user");
            ac.files.uploadChunks(uri,file,totalChunks,chunkSize,progress).then((response)=>{
              console.log("file uploaded: "+file);
              return resolve(response);
            },(err)=>{
              return reject(err);
            });
          },(err)=>{
            return reject(err);
          });
        },(err)=>{
          return reject(err);
        });
      });
    },
    //--------------------------------------------------------------------------
    uploadChunks: (uri,file,total,chunkSize,progress)=>{
      return new Promise ((resolve,reject)=>{
        var chunkIndex = 1;
        var reader = fs.createReadStream(file, { highWaterMark: (chunkSize*1024) });
        reader.on('data', function (chunk) {
          reader.pause();
          var size = Buffer.byteLength(chunk);
          var form = {
            content: chunk,
            index: chunkIndex,
            compressionMode: "NORMAL",
            compressionSize: size,
            originalSize: size
          };
          console.log("uploading chunk "+(chunkIndex)+" for file "+file+" of size "+size);
          if (ac.files.checkCancelled(file)) return resolve("cancelled by user");
          ac.api.chunk("/"+uri+"?returnEntity=true",form).then((response)=>{
            if (response.errors) {
              console.log(response.errors);
              return reject(response.errors);
            }
            chunkIndex++;
            if (progress) progress(Number(100 * (chunkIndex - 1) / total).toFixed(2));
            if (chunkIndex-1==total) {
              reader.close();
              return resolve(response);
            }
            reader.resume();
          },(err)=>{
            console.log(err);
            return reject(err);
          });
        });
      });
    },
    //--------------------------------------------------------------------------
    cancelUpload:(file)=>{
      ac.files.cancelledUploads.push(file);
    },
    //--------------------------------------------------------------------------
  };
  //----------------------------------------------------------------------------
}
module.exports=ACCELLION_CUSTOM;
