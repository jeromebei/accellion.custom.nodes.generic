const fs=require("fs");
const path=require("path");
//==============================================================================
var requirex=function(package,node,name,RED) {
  //console.log(`got package ${package}, node ${node}, name ${name}`);
  var configPaths = [
    path.join(RED.settings.userDir,".config.json"),
    path.join(RED.settings.userDir,".config.nodes.json"),
  ];
  var config = null;
  for (var i=0; i<configPaths.length; i++) {
    var configPath = configPaths[i];
    if (fs.existsSync(configPath)) {
      //console.log(`found config in ${configPath}`);
      config = require(configPath);
      break;
    } 
  }

  if (!config) {console.error(`FATAL: could not find node config file`); return null;}
  if (config[package]) {
    return checkFile(config);
  } else if (config["nodes"]) {
    return checkFile (config["nodes"]);
  } else {
    console.error(`FATAL: could not find root node in config file`);
    return null;
  }

  function checkFile(nodes) {
    if (!nodes[package]) {console.error(`FATAL: could not find package ${package}`); return null;}
    if (!nodes[package].nodes) {console.error(`FATAL: could not find nodes object in ${package}`); return null;}
    if (!nodes[package].nodes[node]) {console.error(`FATAL: could not find node ${node}`); return null;}
    if (!nodes[package].nodes[node].file) {console.error(`FATAL: could not find file object in node ${node}`); return null;}
    var folder = path.dirname(nodes[package].nodes[node].file);
    var file = path.join(folder,name+".js");
    if (!fs.existsSync(file)) file = path.join(folder,"lib","kw",name+".js");
    if (!fs.existsSync(file)) {console.error(`FATAL: could not find file ${file}`); return null;}
    console.log(`found library file ${file}`);
    return require(file);
  }

}
//==============================================================================
module.exports=requirex;
