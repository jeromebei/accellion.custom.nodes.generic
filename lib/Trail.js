//==============================================================================
const Trail={
    //============================================================================
    TYPE:{"SILLY":1, "DEBUG":2, "INFO":3,"WARNING":4,"ERROR":5,"CRASH":6},
    ACTION:{"FORWARD":1, "PROCESS":2, "HALT":3,"WAITING":4,"OTHER":5},
    INVERTED_TYPE: null,
    INVERTED_ACTION: null,
    __nodes:[],
    //============================================================================
    register:(node,config)=>{
      if (!Trail.INVERTED_TYPE) Trail.INVERTED_TYPE=Trail.__invert(Trail.TYPE);
      if (!Trail.INVERTED_ACTION) Trail.INVERTED_ACTION=Trail.__invert(Trail.ACTION);
      Trail.deregister(node);
  
      config.nodeActions=[];
      if (config.actionForward)  config.nodeActions.push(Trail.ACTION.FORWARD);
      if (config.actionProcess)  config.nodeActions.push(Trail.ACTION.PROCESS);
      if (config.actionHalt)     config.nodeActions.push(Trail.ACTION.HALT);
      if (config.actionWaiting)  config.nodeActions.push(Trail.ACTION.WAITING);
      if (config.actionOther)    config.nodeActions.push(Trail.ACTION.OTHER);
      config.messageType=parseInt(config.messageType);
  
      Trail.__nodes.push({node:node,config:config});
  
    },
    //============================================================================
    deregister:(node)=>{
      Trail.__nodes=Trail.__nodes.filter((n)=>{return n.node.id!=node.id});
    },
    //============================================================================
    capture: (type,action,node,message,obj,clearStatus,RED)=>{
      if (clearStatus && RED) {
        RED.nodes.eachNode((__clear_node)=>{
          if (__clear_node.z==node.z){
            __clear_node=RED.nodes.getNode(__clear_node.id);
            __clear_node.status({});
          }
        });
      }
      if (Trail.__nodes.length==0) return;
      var ____nodes = Trail.__nodes.filter((n)=>{
        if (type<n.config.messageType) return false;
        if (!n.config.nodeActions.includes(action)) return false;
        if (n.config.nodeFilter && n.config.nodeFilter!="" && !node.type.match(new RegExp(n.config.nodeFilter))) return false;
        if (!n.node.trail) return false;
        if (n.config.onlyTab && n.node.z!=node.z) return false;
        return true;
      });
      for (var i = 0; i < ____nodes.length; i++) {
        ____nodes[i].node.trail({
          type:type,
          typeDC:Trail.INVERTED_TYPE[type],
          action:action,
          actionDC:Trail.INVERTED_ACTION[action],
          node:node,
          message:message,
          obj: obj
        });
      }
    },
    //============================================================================
    silly: {
      forward:(node,message,obj)=>  {Trail.capture(Trail.TYPE.SILLY,Trail.ACTION.FORWARD,node,message,obj);},
      process:(node,message,obj)=>  {Trail.capture(Trail.TYPE.SILLY,Trail.ACTION.PROCESS,node,message,obj);},
      halt:(node,message,obj)=>     {Trail.capture(Trail.TYPE.SILLY,Trail.ACTION.HALT,node,message,obj);},
      waiting:(node,message,obj)=>  {Trail.capture(Trail.TYPE.SILLY,Trail.ACTION.WAITING,node,message,obj);},
      other:(node,message,obj)=>    {Trail.capture(Trail.TYPE.SILLY,Trail.ACTION.OTHER,node,message,obj);},
    },
    //============================================================================
    debug: {
      forward:(node,message,obj)=>  {Trail.capture(Trail.TYPE.DEBUG,Trail.ACTION.FORWARD,node,message,obj);},
      process:(node,message,obj)=>  {Trail.capture(Trail.TYPE.DEBUG,Trail.ACTION.PROCESS,node,message,obj);},
      halt:(node,message,obj)=>     {Trail.capture(Trail.TYPE.DEBUG,Trail.ACTION.HALT,node,message,obj);},
      waiting:(node,message,obj)=>  {Trail.capture(Trail.TYPE.DEBUG,Trail.ACTION.WAITING,node,message,obj);},
      other:(node,message,obj)=>    {Trail.capture(Trail.TYPE.DEBUG,Trail.ACTION.OTHER,node,message,obj);},
    },
    //============================================================================
    info: {
      forward:(node,message,obj)=>  {Trail.capture(Trail.TYPE.INFO,Trail.ACTION.FORWARD,node,message,obj);},
      process:(node,message,obj)=>  {Trail.capture(Trail.TYPE.INFO,Trail.ACTION.PROCESS,node,message,obj);},
      halt:(node,message,obj)=>     {Trail.capture(Trail.TYPE.INFO,Trail.ACTION.HALT,node,message,obj);},
      waiting:(node,message,obj)=>  {Trail.capture(Trail.TYPE.INFO,Trail.ACTION.WAITING,node,message,obj);},
      other:(node,message,obj)=>    {Trail.capture(Trail.TYPE.INFO,Trail.ACTION.OTHER,node,message,obj);},
    },
    //============================================================================
    warning: {
      forward:(node,message,obj)=>  {Trail.capture(Trail.TYPE.WARNING,Trail.ACTION.FORWARD,node,message,obj);},
      process:(node,message,obj)=>  {Trail.capture(Trail.TYPE.WARNING,Trail.ACTION.PROCESS,node,message,obj);},
      halt:(node,message,obj)=>     {Trail.capture(Trail.TYPE.WARNING,Trail.ACTION.HALT,node,message,obj);},
      waiting:(node,message,obj)=>  {Trail.capture(Trail.TYPE.WARNING,Trail.ACTION.WAITING,node,message,obj);},
      other:(node,message,obj)=>    {Trail.capture(Trail.TYPE.WARNING,Trail.ACTION.OTHER,node,message,obj);},
    },
    //============================================================================
    error: {
      forward:(node,message,obj)=>  {Trail.capture(Trail.TYPE.ERROR,Trail.ACTION.FORWARD,node,message,obj);},
      process:(node,message,obj)=>  {Trail.capture(Trail.TYPE.ERROR,Trail.ACTION.PROCESS,node,message,obj);},
      halt:(node,message,obj)=>     {Trail.capture(Trail.TYPE.ERROR,Trail.ACTION.HALT,node,message,obj);},
      waiting:(node,message,obj)=>  {Trail.capture(Trail.TYPE.ERROR,Trail.ACTION.WAITING,node,message,obj);},
      other:(node,message,obj)=>    {Trail.capture(Trail.TYPE.ERROR,Trail.ACTION.OTHER,node,message,obj);},
    },
    //============================================================================
    crash: {
      forward:(node,message,obj)=>  {Trail.capture(Trail.TYPE.CRASH,Trail.ACTION.FORWARD,node,message,obj);},
      process:(node,message,obj)=>  {Trail.capture(Trail.TYPE.CRASH,Trail.ACTION.PROCESS,node,message,obj);},
      halt:(node,message,obj)=>     {Trail.capture(Trail.TYPE.CRASH,Trail.ACTION.HALT,node,message,obj);},
      waiting:(node,message,obj)=>  {Trail.capture(Trail.TYPE.CRASH,Trail.ACTION.WAITING,node,message,obj);},
      other:(node,message,obj)=>    {Trail.capture(Trail.TYPE.CRASH,Trail.ACTION.OTHER,node,message,obj);},
    },
    //============================================================================
    s:{
      f:(node,message,obj)=>{Trail.silly.forward(node,message,obj);},
      p:(node,message,obj)=>{Trail.silly.process(node,message,obj);},
      h:(node,message,obj)=>{Trail.silly.halt(node,message,obj);},
      w:(node,message,obj)=>{Trail.silly.waiting(node,message,obj);},
      o:(node,message,obj)=>{Trail.silly.other(node,message,obj);},
    },
    //============================================================================
    d:{
      f:(node,message,obj)=>{Trail.debug.forward(node,message,obj);},
      p:(node,message,obj)=>{Trail.debug.process(node,message,obj);},
      h:(node,message,obj)=>{Trail.debug.halt(node,message,obj);},
      w:(node,message,obj)=>{Trail.debug.waiting(node,message,obj);},
      o:(node,message,obj)=>{Trail.debug.other(node,message,obj);},
    },
    //============================================================================
    i:{
      f:(node,message,obj)=>{Trail.info.forward(node,message,obj);},
      p:(node,message,obj)=>{Trail.info.process(node,message,obj);},
      h:(node,message,obj)=>{Trail.info.halt(node,message,obj);},
      w:(node,message,obj)=>{Trail.info.waiting(node,message,obj);},
      o:(node,message,obj)=>{Trail.info.other(node,message,obj);},
    },
    //============================================================================
    w:{
      f:(node,message,obj)=>{Trail.warning.forward(node,message,obj);},
      p:(node,message,obj)=>{Trail.warning.process(node,message,obj);},
      h:(node,message,obj)=>{Trail.warning.halt(node,message,obj);},
      w:(node,message,obj)=>{Trail.warning.waiting(node,message,obj);},
      o:(node,message,obj)=>{Trail.warning.other(node,message,obj);},
    },
    //============================================================================
    e:{
      f:(node,message,obj)=>{Trail.error.forward(node,message,obj);},
      p:(node,message,obj)=>{Trail.error.process(node,message,obj);},
      h:(node,message,obj)=>{Trail.error.halt(node,message,obj);},
      w:(node,message,obj)=>{Trail.error.waiting(node,message,obj);},
      o:(node,message,obj)=>{Trail.error.other(node,message,obj);},
    },
    //============================================================================
    c:{
      f:(node,message,obj)=>{Trail.crash.forward(node,message,obj);},
      p:(node,message,obj)=>{Trail.crash.process(node,message,obj);},
      h:(node,message,obj)=>{Trail.crash.halt(node,message,obj);},
      w:(node,message,obj)=>{Trail.crash.waiting(node,message,obj);},
      o:(node,message,obj)=>{Trail.crash.other(node,message,obj);},
    },
    //============================================================================
    __invert:(json)=>{
      var ret = {};
      for(var key in json)ret[json[key]] = key;
      return ret;
    },
    //============================================================================
  }
  module.exports=Trail;
  