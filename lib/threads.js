//==============================================================================
var Threads = {
  THREAD_STATE:{"CREATED":1, "RUNNING":2, "DONE":3,"ERROR":4},
  POOL_STATE:{"RUNNING":1, "STOPPED":2},
  //============================================================================
  __pools : [],
  //============================================================================
  createPool:(name,maxThreads)=>{
    var pool = {
      state: Threads.POOL_STATE.STOPPED,
      name:name,
      maxThreads:maxThreads,
      cb:null,
      queue:[],
      __idCount:0,
      __watchInterval: 150,
      __stopping: false,
      //------------------------------------------------------------------------
      add: (fn,context)=>{Threads.addThread(name,fn,context)},
      remove: (id)=>{Threads.removeThread(name,id)},
      run: (cb)=>{return Threads.runPool(name,cb)},
      stop: ()=>{return Threads.stopPool(name)},
      running: ()=>{return Threads.getPool(name).queue.filter((t)=>{return t.state==Threads.THREAD_STATE.RUNNING;}).length},
      remaining: ()=>{return Threads.getPool(name).queue.filter((t)=>{return t.state==Threads.THREAD_STATE.CREATED}).length},
      done: ()=>{return Threads.getPool(name).queue.filter((t)=>{return t.state==Threads.THREAD_STATE.RUNNING || t.state==Threads.THREAD_STATE.CREATED}).length==0},
      setSize: (maxThreads)=>{return Threads.setSize(name,maxThreads)},
      //------------------------------------------------------------------------
    };
    if (Threads.getPool(name)) {Threads.stopPool(name); Threads.deletePool(name);}
    Threads.__pools.push(pool);
    return pool;
  },
  //============================================================================
  deletePool:(name)=>{
    Threads.__pools=Threads.__pools.filter((n)=>{return n.name!=name});
  },
  //============================================================================
  setSize:(name, maxThreads)=>{
    var pool = Threads.getPool(name);
    if (pool) pool.maxThreads=maxThreads;
    console.log("pool "+pool.name+" changed size to "+pool.maxThreads);

    return pool;
  },
  //============================================================================
  getPool:(name)=>{
    return Threads.__pools.find((n)=>{return n.name==name});
  },
  //============================================================================
  addThread:(name,fn,context)=>{
    var pool = Threads.getPool(name);
    if (pool) {
      thread = new Thread(fn,context,pool.__idCount);
      pool.queue.push(thread);
      pool.__idCount++;
      return thread.id;
    } else {
      return -1;
    }
  },
  //============================================================================
  removeThread:(name,id)=>{
    var pool = Threads.getPool(name);
    if (pool) pool=pool.filter((n)=>{return n.id!=id});
  },
  //============================================================================
  runPool:(name,cb)=>{
    var pool = Threads.getPool(name);
    if (!pool) return cb({error:"pool not found"});
    console.log("running pool "+pool.name+" with size "+pool.maxThreads);
    pool.state=Threads.POOL_STATE.RUNNING;
    pool.__stopping=false;
    pool.cb=cb;
    (function __run(){

      while (pool.running() < pool.maxThreads && pool.remaining() > 0){
        nextThread = pool.queue.find((t)=>{return t.state==Threads.THREAD_STATE.CREATED});
        nextThread.state=Threads.THREAD_STATE.RUNNING;
        nextThread.fn(nextThread,nextThread.context,(thread,err)=>{
          if (err) thread.state=Threads.THREAD_STATE.ERROR;
          else thread.state=Threads.THREAD_STATE.DONE;
          pool.queue=pool.queue.filter((t)=>{return t.id!=thread.id});
        });
      }
      if (!pool.__stopping) setTimeout(__run,pool.__watchInterval);
      else pool.__stopping=false;
    })();
    return pool;
  },
  //============================================================================
  stopPool:(name)=>{
    var pool = Threads.getPool(name);
    pool.state=Threads.POOL_STATE.STOPPED;
    if (pool.cb) pool.cb();
    pool.__stopping=true;
    return pool;
  },
  //============================================================================
  stopAllPools:()=>{
    for (var i = 0; i < Threads.__pools.length; i++) {
      Threads.__pools[i].stop();
    }
  },
  //============================================================================
}
//==============================================================================
class Thread {
  //============================================================================
  constructor(fn,context,id) {
    this.state=Threads.THREAD_STATE.CREATED;
    this.fn=fn;
    this.context=context;
    this.id=id;
  }
  //============================================================================
}

//==============================================================================
module.exports=Threads;
